import constants from './constants'

export default function (
    title: string,
    description: string,
    imageLink: string
  ) {
    const imageUrl =
      typeof imageLink === 'string' ? imageLink : constants.socialMediaImageUrl
  
    return [
      {
        prefix: 'og: http://ogp.me/ns#',
        property: 'og:image',
        content: imageUrl
      },
      {
        hid: 'description',
        name: 'description',
        content: description
      },
      {
        name: 'author',
        content: 'VHEC'
      },
      // Facebook
      {
        property: 'og:title',
        content: title
      },
      {
        property: 'og:site_name',
        content: 'vhec.vn'
      },
    ]
  }