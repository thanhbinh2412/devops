import ConnectionInstance from '~/plugins/connection'

export const getListCategory = () => {
  return ConnectionInstance.get('/category')
}

export const newCategory = (req) => {
  return ConnectionInstance.post('/category', {
    category_name: req.category_name,
    description: req.description,
  })
}

export const getListPostAdd = () => {
  return ConnectionInstance.get('/category/list-post-add')
}

export const getListPostBySeries = (id) => {
  return ConnectionInstance.get(`/category/list-post-series/${id}`)
}

export const addCategoryInPost = (body) => {
  return ConnectionInstance.put('/category/add-category-in-post', {
    category_id: body.category_id,
    ma_post: body.ma_post,
  })
}
