# FROM node:14.7

# ENV APP_ROOT /src

# RUN mkdir ${APP_ROOT}
# WORKDIR /${APP_ROOT}
# ADD . ${APP_ROOT}

# RUN npm install
# RUN npm run dev
# # RUN npm run build


# ENV HOST 0.0.0.0


# build stage
FROM node:16 as build-stage
WORKDIR /app
COPY . .
RUN npm install
# RUN npm run build
RUN npm run generate

# production stage
FROM nginx:1.17-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]