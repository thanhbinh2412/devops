import Prism from 'prismjs'
import 'prismjs/themes/prism-tomorrow.css' // You can add other themes if you want
import 'prismjs/components/prism-bash'
import 'prismjs/components/prism-javascript'
import 'prismjs/components/prism-typescript'
import 'prismjs/components/prism-powershell'
import 'prismjs/themes/prism-solarizedlight.css'
